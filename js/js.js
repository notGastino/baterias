//alert('ok');
 
//  objeto JSON com os dados
var dados = [
    {
      pilha: "Pilha Alcalina Duracel AAA",
      tensao_nominal: 1.5,
      corrente: 0.0418,
      E: 0.865,
      V: 0.987,
      R: 23.6,
    },
    {
      pilha: "Pilha Panasonic AA LR6",
      tensao_nominal: 1.5,
      corrente: 0.0584,
      E: 1.343,
      V: 1.379,
      R: 23.6,
    },
    {
      pilha: "Pilha Phillips AAA LRO3",
      tensao_nominal: 1.5,
      corrente: 0.0578,
      E: 1.327,
      V: 1.371,
      R: 23.7,
    },
    {
      pilha: "Bateria Luatek",
      tensao_nominal: 3.7,
      corrente: 0.1062,
      E: 2.430,
      V: 2.517,
      R: 23.7,
    },
    {
        pilha: "Bateria Jyx 9600mah SD18650",
        tensao_nominal: 4.2,
        corrente: 0.4000,
        E: 2.079,
        V: 9.479,
        R: 23.5,
    },
    {
        pilha: "Bateria Elgin 250mah",
        tensao_nominal: 9,
        corrente: 0.3055,
        E: 2.134,
        V: 7.21,
        R: 23.6,
      },
      {
        pilha: "Bateria Golite 6722",
        tensao_nominal: 9,
        corrente: 0.2491,
        E: 266.5,
        V: 5.88,
        R: 23.6,
      },
      {
        pilha: "Bateria Freedom DF300",
        tensao_nominal: 9,
        corrente: 0.0454,
        E: 1055,
        V: 1068,
        R: 23.5,
      },
      {
        pilha: "Bateria Unipower",
        tensao_nominal: 12,
        corrente: 0.0446,
        E: 1030,
        V: 1049,
        R: 23.5,
      },
  ];
   
  // Função para calcular a resistencia interna de cada medição e preencher a tabela
  function calcularResistencia() {
    var tabela = document.getElementById("tbDados");
   
    // Limpar tabela antes de preencher
    var tabelaHTML = `<tr>
                              <th>Pilha/Bateria</th>
                              <th>Tensão nominal (V)</th>
                              <th>Capacidade de corrente (mA.h)</th>
                              <th>Tensão sem carga(V)</th>
                              <th>Tensão com carga(V)</th>
                              <th>Resitência de carga(ohm)</th>
                              <th>Resistência interna(ohm)</th>
                    </tr>`;
   
    // Iterar sobre os dados e calcular o r de cada um
    for (var i = 0; i < dados.length; i++) {
      var linha = dados[i];
      var E = linha.E;
      var V = linha.V;
      var R = linha.R;
      var r = R * (E / V - 1);
   
      // Adicionar nova linha à tabela com os valores e a soma
      var novaLinha = "<tr><td>" + linha.pilha + "</td><td>" + linha.tensao_nominal + "</td><td>" + linha.corrente + "</td><td>" + linha.E + "</td><td>" + linha.V + "</td><td>" + linha.R + "</td><td>" + r.toFixed(4) + "</td></tr>";
   
      tabelaHTML += novaLinha;
    }
    tabela.innerHTML = tabelaHTML;
  }
   
  calcularResistencia();